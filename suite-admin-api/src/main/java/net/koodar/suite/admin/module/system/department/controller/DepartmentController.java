package net.koodar.suite.admin.module.system.department.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.koodar.suite.admin.module.system.department.model.entity.Department;
import net.koodar.suite.admin.module.system.department.model.params.DepartmentParam;
import net.koodar.suite.admin.module.system.department.model.vo.DepartmentVo;
import net.koodar.suite.admin.module.system.department.service.DepartmentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门接口
 *
 * @author liyc
 */
@Tag(name = "DeptController", description = "系统管理-部门")
@RestController
public class DepartmentController {

	private final DepartmentService departmentService;

	public DepartmentController(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}

	@GetMapping("/department/tree")
	@Operation(summary = "获取部门列表")
	public List<DepartmentVo> deptTree() {
		List<Department> departmentList = departmentService.getAllList();
		List<Department> rootList = departmentList.stream().filter(permission -> permission.getParentId() != null && permission.getParentId() <= 0).collect(Collectors.toList());
		return departmentService.toTree(rootList, departmentList);
	}

	@Operation(summary = "添加部门")
	@PostMapping("/department/add")
	public void addDepartment(@RequestBody DepartmentParam departmentParam) {
		departmentService.addDepartment(departmentParam);
	}

	@Operation(summary = "修改部门")
	@PostMapping("/department/update")
	public void updDepartment(@RequestBody DepartmentParam departmentParam) {
		departmentService.updateDepartment(departmentParam);
	}

	@Operation(summary = "删除部门")
	@PostMapping("/department/delete/{id}")
	public void delDepartment(@PathVariable("id") Long id) {
		departmentService.deleteDepartment(id);
	}


}
