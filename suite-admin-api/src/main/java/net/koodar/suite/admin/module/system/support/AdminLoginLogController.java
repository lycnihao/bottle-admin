package net.koodar.suite.admin.module.system.support;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import net.koodar.suite.common.support.loginlog.LoginLogService;
import net.koodar.suite.common.support.loginlog.model.entity.LoginLog;
import net.koodar.suite.common.support.loginlog.model.params.LoginLogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.data.domain.Sort.Direction.DESC;

/**
 * 登录日志
 *
 * @author liyc
 */
@Tag(name = "AdminLoginLogController", description = "登录日志")
@RestController
@RequiredArgsConstructor
public class AdminLoginLogController {

	private final LoginLogService loginLogService;

	@Operation(summary = "查询登录日志列表")
	@GetMapping("/loginLog/list")
	public Page<LoginLog> getUserList(
			@PageableDefault(sort = {"createTime"}, direction = DESC) Pageable pageable,
			LoginLogQuery loginLogQuery) {
		return loginLogService.pageBy(loginLogQuery, pageable);
	}

}
