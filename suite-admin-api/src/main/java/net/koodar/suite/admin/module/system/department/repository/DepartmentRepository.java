package net.koodar.suite.admin.module.system.department.repository;

import net.koodar.suite.admin.module.system.department.model.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 部门仓储接口
 *
 * @author liyc
 */
public interface DepartmentRepository extends JpaRepository<Department, Long> {

	int countByParentIdAndDeptName(Long parentId, String deptName);

	Department findFirstByParentIdAndDeptName(Long parentId, String deptName);
}
