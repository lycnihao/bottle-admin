package net.koodar.suite.admin.module.system.user.model.params;

import lombok.Data;

import java.util.List;

/**
 * 更新部门
 *
 * @author liyc
 */
@Data
public class UpdDepartmentParam {

	private List<Long> userIds;

	private Long departmentId;

}
