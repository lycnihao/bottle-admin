package net.koodar.suite.admin.module.system.user.model.params;

import lombok.Data;

/**
 * User query.
 *
 * @author liyc
 */
@Data
public class UserQuery {

	private String username;

	private Long departmentId;

}
