package net.koodar.suite.admin.module.system.department.model.vo;

import lombok.Data;
import net.koodar.suite.admin.module.system.department.model.entity.Department;

import java.util.List;

/**
 * Dept Tree
 *
 * @author liyc
 */
@Data
public class DepartmentVo extends Department {

	private List<DepartmentVo> children;

}
