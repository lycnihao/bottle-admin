package net.koodar.suite.admin.module.system.department.model.params;

import lombok.Data;

/**
 * Dept 参数
 *
 * @author liyc
 */
@Data
public class DepartmentParam {

	private Long id;

	private Long parentId = 0L;

	private String deptName;

	private Integer sort;

}
