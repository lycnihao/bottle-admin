package net.koodar.suite.admin.module.system.role.model.params;

import lombok.Data;

/**
 * @author liyc
 */
@Data
public class RoleQuery {

	private String roleName;

}
